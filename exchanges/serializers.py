from rest_framework import serializers
from .models import ExchangeRate, DailyExchangeRate


class ExchangeRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExchangeRate
        fields = ('id', 'rate_from', 'rate_to')


class DailyExchangeRateSerializer(serializers.ModelSerializer):
    exchange_rate = serializers\
        .PrimaryKeyRelatedField(queryset=ExchangeRate.objects)
    rate_from = serializers.CharField(source='exchange_rate.rate_from',
                                      read_only=True)
    rate_to = serializers.CharField(source='exchange_rate.rate_to',
                                    read_only=True)

    class Meta:
        model = DailyExchangeRate
        fields = ('id', 'rate', 'created_at', 'exchange_rate',
                  'rate_from', 'rate_to')
        depth = 1
