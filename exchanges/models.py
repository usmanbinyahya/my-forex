from django.db import models


class ExchangeRate(models.Model):
    rate_from = models.CharField(max_length=50)
    rate_to = models.CharField(max_length=50)


class DailyExchangeRate(models.Model):
    created_at = models.DateField(auto_now_add=True)
    rate = models.DecimalField(max_digits=20, decimal_places=2)
    exchange_rate = models.ForeignKey(ExchangeRate, on_delete=models.PROTECT)
