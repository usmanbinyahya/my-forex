from django.db.models import Avg
from rest_framework import viewsets, status
from rest_framework.response import Response
import datetime
from .models import ExchangeRate, DailyExchangeRate
from .serializers import (
    ExchangeRateSerializer, DailyExchangeRateSerializer
)
from rest_framework.views import APIView


class HomeView(APIView):
    def get(self, request):
        return Response('Hello Forex')


class ExchangeRateSet(viewsets.ModelViewSet):
    queryset = ExchangeRate.objects.all()
    serializer_class = ExchangeRateSerializer


class DailyExchangeRateSet(viewsets.ModelViewSet):
    serializer_class = DailyExchangeRateSerializer
    queryset = DailyExchangeRate.objects.all()


class ListExchangeView(APIView):
    def get(self, request):
        input_date = request.query_params.get('date', None)

        if input_date is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            date = datetime.datetime.strptime(input_date, '%Y-%m-%d')
        except ValueError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        last_seven_day = date - datetime.timedelta(days=7)

        result = DailyExchangeRate.objects \
            .filter(created_at__range=(last_seven_day, date)) \
            .values('exchange_rate') \
            .annotate(avg_rate=Avg('rate')) \
            .order_by('-created_at')

        recon = DailyExchangeRate.objects\
            .filter(created_at=date).distinct('exchange_rate')

        data = []
        for rc in recon:
            for r in result:
                if rc.exchange_rate.id == r.get('exchange_rate'):
                    data.append({
                        'id': rc.id,
                        'created_at': str(rc.created_at),
                        'rate': str(rc.rate),
                        'avg_rate': str(r.get('avg_rate')),
                        'rate_from': rc.exchange_rate.rate_from,
                        'rate_to': rc.exchange_rate.rate_to,
                    })
                else:
                    data.append({
                        'id': rc.id,
                        'created_at': str(rc.created_at),
                        'rate': 'insufficient data',
                        'avg_rate': 'insufficient data',
                        'rate_from': rc.exchange_rate.rate_from,
                        'rate_to': rc.exchange_rate.rate_to,
                    })

        return Response(data)
