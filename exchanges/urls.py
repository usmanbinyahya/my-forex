from rest_framework import routers
from .views import ExchangeRateSet, DailyExchangeRateSet, ListExchangeView
from django.urls import path


app_name = 'exchanges'

router = routers.SimpleRouter()
router.register('exchanges', ExchangeRateSet, base_name='exchange')
router.register('daily-exchanges', DailyExchangeRateSet,
                base_name='daily-exchange')

urlpatterns = router.urls

urlpatterns += [
    path('list-exchanges/', ListExchangeView.as_view()),
]
