from rest_framework.test import APITestCase
from model_mommy import mommy
import datetime


class ExchangeRateSetTest(APITestCase):
    def setUp(self):
        self._rate_from = 'USD'
        self._rate_to = 'IDR'
        self._url = '/api/v1/exchanges/'

    def test_can_add_exchange_rate(self):
        data = {
            'rate_from': self._rate_from,
            'rate_to': self._rate_to,
        }

        response = self.client.post(self._url, data)
        result = response.data

        self.assertEqual(response.status_code, 201)
        self.assertEqual(result.get('rate_from'), self._rate_from)
        self.assertEqual(result.get('rate_to'), self._rate_to)
        self.assertIsNotNone(result.get('id'))

    def test_can_get_all_exchange_rate(self):
        mommy.make('exchanges.ExchangeRate',
                   rate_from=self._rate_from, rate_to=self._rate_to)

        response = self.client.get(self._url)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].get('rate_from'), self._rate_from)
        self.assertEqual(result[0].get('rate_to'), self._rate_to)
        self.assertIsNotNone(result[0].get('id'))

    def test_can_get_one_exchange_rate(self):
        rate = mommy.make('exchanges.ExchangeRate',
                          rate_from=self._rate_from, rate_to=self._rate_to)
        url = f'{self._url}{rate.pk}/'
        response = self.client.get(url)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(result.get('rate_from'), self._rate_from)
        self.assertEqual(result.get('rate_to'), self._rate_to)
        self.assertIsNotNone(result.get('id'))

    def test_can_edit_exchange_rate(self):
        rate = mommy.make('exchanges.ExchangeRate',
                          rate_from=self._rate_from, rate_to=self._rate_to)
        url = f'{self._url}{rate.pk}/'

        data = {
            'rate_from': self._rate_to,
            'rate_to': self._rate_from,
        }

        response = self.client.put(url, data)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(result.get('rate_from'), self._rate_to)
        self.assertEqual(result.get('rate_to'), self._rate_from)
        self.assertIsNotNone(result.get('id'))

    def test_can_edit_partial_exchange_rate(self):
        rate = mommy.make('exchanges.ExchangeRate',
                          rate_from=self._rate_from, rate_to=self._rate_to)
        url = f'{self._url}{rate.pk}/'

        change_from = 'EUR'
        data = {
            'rate_from': change_from,
        }

        response = self.client.patch(url, data)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(result.get('rate_from'), change_from)
        self.assertEqual(result.get('rate_to'), self._rate_to)
        self.assertIsNotNone(result.get('id'))

    def test_can_remove_exchange_rate(self):
        rate = mommy.make('exchanges.ExchangeRate',
                          rate_from=self._rate_from, rate_to=self._rate_to)
        url = f'{self._url}{rate.pk}/'

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)
        self.assertIsNone(response.data)


class DailyExchangeRateSetTest(APITestCase):
    def setUp(self):
        self._rate_from = 'USD'
        self._rate_to = 'IDR'
        self._rate = 14500.00
        self._url = '/api/v1/daily-exchanges/'

        self._ex_rate = mommy.make('exchanges.ExchangeRate',
                                   rate_from=self._rate_from,
                                   rate_to=self._rate_to)

    def test_can_get_all_daily_exchange_rate(self):
        mommy.make('exchanges.DailyExchangeRate', rate=self._rate,
                   exchange_rate=self._ex_rate)

        response = self.client.get(self._url)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(float(result[0].get('rate')), self._rate)
        self.assertIsNotNone(result[0].get('created_at'))
        self.assertIsNotNone(result[0].get('id'))
        self.assertEqual(self._ex_rate.id, result[0].get('exchange_rate'))
        self.assertEqual(self._ex_rate.rate_from, result[0].get('rate_from'))
        self.assertEqual(self._ex_rate.rate_to, result[0].get('rate_to'))

    def test_can_get_one_daily_exchange_rate(self):
        model = mommy.make('exchanges.DailyExchangeRate', rate=self._rate,
                           exchange_rate=self._ex_rate)
        url = f'{self._url}{model.pk}/'

        response = self.client.get(url)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(float(result.get('rate')), self._rate)
        self.assertIsNotNone(result.get('created_at'))
        self.assertIsNotNone(result.get('id'))
        self.assertEqual(self._ex_rate.id, result.get('exchange_rate'))
        self.assertEqual(self._ex_rate.rate_from, result.get('rate_from'))
        self.assertEqual(self._ex_rate.rate_to, result.get('rate_to'))

    def test_can_add_daily_exchange_rate(self):
        data = {
            'rate': self._rate,
            'exchange_rate': self._ex_rate.pk,
        }

        response = self.client.post(self._url, data)
        result = response.data

        self.assertEqual(response.status_code, 201)
        self.assertEqual(float(result.get('rate')), self._rate)
        self.assertIsNotNone(result.get('created_at'))
        self.assertEqual(self._rate_from, result.get('rate_from'))
        self.assertEqual(self._rate_to, result.get('rate_to'))
        self.assertIsNotNone(result.get('id'))

    def test_can_edit_daily_exchange_rate(self):
        ex_rate = mommy.make('exchanges.ExchangeRate',
                             rate_from=self._rate_to,
                             rate_to=self._rate_from)
        model = mommy.make('exchanges.DailyExchangeRate', rate=self._rate,
                           exchange_rate=self._ex_rate)
        url = f'{self._url}{model.pk}/'

        rate = 15000.00
        data = {
            'rate': rate,
            'exchange_rate': ex_rate.pk
        }

        response = self.client.put(url, data)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(float(result.get('rate')), rate)
        self.assertIsNotNone(result.get('created_at'))
        self.assertIsNotNone(result.get('id'))
        self.assertEqual(ex_rate.id, result.get('exchange_rate'))
        self.assertEqual(ex_rate.rate_from, result.get('rate_from'))
        self.assertEqual(ex_rate.rate_to, result.get('rate_to'))

    def test_can_edit_partial_daily_exchange_rate(self):
        model = mommy.make('exchanges.DailyExchangeRate', rate=self._rate,
                           exchange_rate=self._ex_rate)
        url = f'{self._url}{model.pk}/'

        rate = 15000.00
        data = {
            'rate': rate
        }

        response = self.client.patch(url, data)
        result = response.data

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(result, dict)
        self.assertEqual(float(result.get('rate')), rate)
        self.assertIsNotNone(result.get('created_at'))
        self.assertIsNotNone(result.get('id'))
        self.assertEqual(self._ex_rate.id, result.get('exchange_rate'))
        self.assertEqual(self._ex_rate.rate_from, result.get('rate_from'))
        self.assertEqual(self._ex_rate.rate_to, result.get('rate_to'))

    def test_can_remove_daily_exchange_rate(self):
        model = mommy.make('exchanges.DailyExchangeRate', rate=self._rate,
                           exchange_rate=self._ex_rate)
        url = f'{self._url}{model.pk}/'

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)
        self.assertIsNone(response.data)


class ListExchangeViewTest(APITestCase):
    def setUp(self):
        self._url = '/api/v1/list-exchanges/'

    def test_get_list_exchange_view(self):
        ex_rates = mommy.make('exchanges.ExchangeRate', _quantity=6)

        today = datetime.date.today()
        for i in range(7):
            delta = today - datetime.timedelta(days=(i + 1))

            for ex_rate in ex_rates:
                mommy.make('exchanges.DailyExchangeRate',
                           created_at=delta, exchange_rate=ex_rate)

        data = {
            'date': today.strftime('%Y-%m-%d'),
        }

        response = self.client.get(self._url, data)

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.data, list)

    def test_confirm_avg_value(self):
        ex_rate = mommy.make('exchanges.ExchangeRate')

        avg = 0.0
        today = datetime.date.today()
        for i in range(7):
            delta = today - datetime.timedelta(days=(i + 1))

            daily = mommy.make('exchanges.DailyExchangeRate',
                               rate=15000.0, created_at=delta,
                               exchange_rate=ex_rate)
            avg += float(daily.rate)

        avg /= 7

        data = {
            'date': today.strftime('%Y-%m-%d'),
        }

        response = self.client.get(self._url, data)
        result = response.data

        self.assertEqual(float(avg), float(result[0].get('avg_rate')))

