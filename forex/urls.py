"""forex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from exchanges.views import HomeView


schema_view = get_schema_view(
        openapi.Info(
                title='Forex API',
                default_version='v1',
                description='Daily & Average Forex'),
        public=True,
        permission_classes=(permissions.AllowAny, ),
        urlconf='forex.urls')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('exchanges.urls', namespace='v1')),
    path('', HomeView.as_view()),
    path('swagger.json', schema_view.without_ui(cache_timeout=None),
         name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=None),
         name='schema-swagger-ui'),
]
